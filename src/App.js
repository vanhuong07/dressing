import React from 'react';
import './App.css';
import HomeDressingRoom from './DressingRoom/Home/home';

function App() {
  return (
    <>
      <HomeDressingRoom/>
    </>
  );
}

export default App;
