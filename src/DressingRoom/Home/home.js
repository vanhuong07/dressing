import React, { Component } from 'react';
import HeaderComponent from '../../Layouts/Header/header';
import FooterComponent from '../../Layouts/Footer/footer';
import CategoriesComponent from '../../Commponents/Categories/categories';
import ProductsComponent from '../../Commponents/Products/products';
import ModelComponent from '../../Commponents/Model/model';

class HomeDressingRoom extends Component {
    render() {
        return (
            <div>
                <HeaderComponent/>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-6'>
                            <CategoriesComponent/>
                            <ProductsComponent/>
                        </div>
                        <div className='col-6'>
                            <ModelComponent/>
                        </div>
                    </div>
                </div>
                <FooterComponent/>
            </div>
        );
    }
}

export default HomeDressingRoom;