let initialState = {
    topclothes: '',
    botclothes: '',
    shoes: '',
    handbags: '',
    necklaces: '',
    hairstyle: '',
    background: '',
}

const ModelReducer = (state = initialState , action) => {
    switch (action.type) {
        case 'CHOOSE_CLOTHES': 
            state[action.payload.type] = action.payload.image;
        return { ...state };
        default: 
        return state;
    }
}
export default ModelReducer;