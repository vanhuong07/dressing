import {combineReducers} from 'redux';
import ProductReducer from './product';
import CategoryReducer from './categori';
import filterCategoryReducer from './filter';
import ModelReducer from './model';
const RootReducer = combineReducers ({
    products: ProductReducer,
    categories: CategoryReducer,
    filterType: filterCategoryReducer,
    setModel: ModelReducer,
})


export default RootReducer; 