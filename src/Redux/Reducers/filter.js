let initialState = 'topclothes';

const filterCategoryReducer = (state = initialState, action) => {
    const {type} = action;
    switch (type){
        case "FILTER_CLOTHES":
            state = action.payload
            return state;
        default:
            return state;
    }
}

export default filterCategoryReducer;