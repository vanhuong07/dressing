import React, { Component } from "react";
import {connect} from 'react-redux';
class ProductsItemComponent extends Component {
  render() {
    const {imgSrc_jpg, name} = this.props.item;
    return (
      <>
        <div className='card p-2 my-2'>
          <img src={imgSrc_jpg} alt='products-item'/>
          <p>{name}</p>
          <button onClick={() => this.setClothes(this.props.item)} className="btn btn-success">Thử Đồ</button>
        </div>
      </>
    );
  }
  setClothes = (clothes) => {
    this.props.dispatch({
      type: 'CHOOSE_CLOTHES',
      payload: {
        type: clothes.type,
        image: clothes.imgSrc_png,
      }
    })
  }
}

export default connect()(ProductsItemComponent);
