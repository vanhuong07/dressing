import React, { Component } from "react";
import ProductsItemComponent from "../ProductsItem/productsItem";
import { connect } from "react-redux";
class ProductsComponent extends Component {
  render() {
    return (
      <>
        <div className="row">
          {this.props.productList.filter(item => item.type === this.props.filterType).map((item, index) => (
            <div className="col-4" key={index}>
              <ProductsItemComponent item={item} />
            </div>
          ))}
        </div>
      </>
    );
  }
}

const MapStateToProps = (state) => ({
  productList: state.products,
  filterType: state.filterType,
});

export default connect(MapStateToProps)(ProductsComponent);
