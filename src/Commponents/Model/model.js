import React, { Component } from "react";
import classes from "./style.module.css";
import { connect } from "react-redux";
class ModelComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contain: "images/background/background_998.jpg",
      body: "images/allbody/bodynew.png",
      bikinitop: "images/allbody/bikini_branew.png",
      bikinibottom: "images/allbody/bikini_pantsnew.png",
      model: "images/model/1000new.png",
      feedleft: "images/allbody/feet_high_leftnew.png",
      feedright: "images/allbody/feet_high_rightnew.png",
    };
  }
  render() {
    return (
      <div>
        <div
          className={classes.contain}
          style={{ background: `url(${this.state.contain})` }}
        >
          <div
            className={classes.body}
            style={{ background: `url(${this.state.body})` }}
          />
          <div
            className={classes.model}
            style={{ background: `url(${this.state.model})` }}
          />
          <div
            className={classes.bikinitop}
            style={{ background: `url(${this.state.bikinitop})` }}
          />
          <div
            className={classes.bikinibottom}
            style={{ background: `url(${this.state.bikinibottom})` }}
          />
          <div
            className={classes.feetleft}
            style={{ background: `url(${this.state.feedleft})` }}
          />
          <div
            className={classes.feetright}
            style={{ background: `url(${this.state.feedright})` }}
          />
          <div className={classes.bikinitop} style={{backgroundImage: `url(${this.props.setModel.topclothes})`, backgroundSize: 'cover'}}></div>
          <div className={classes.bikinibottom} style={{backgroundImage: `url(${this.props.setModel.botclothes})`, backgroundSize: 'cover'}}></div>
          <div className={classes.hairStyle} style={{backgroundImage: `url(${this.props.setModel.hairstyle})`, backgroundSize: 'cover'}}></div>
          <div className={classes.handbagStyle} style={{backgroundImage: `url(${this.props.setModel.handbags})`, backgroundSize: 'cover'}}></div>
          <div className={classes.neckStyle} style={{backgroundImage: `url(${this.props.setModel.necklaces})`, backgroundSize: 'cover'}}></div>
          <div className={classes.shoesStyle} style={{backgroundImage: `url(${this.props.setModel.shoes})`, backgroundSize: 'cover'}}></div>
          <div className={classes.contain} style={{backgroundImage: `url(${this.props.setModel.background})`, backgroundSize: 'cover'}}></div> 
        </div>


      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  setModel: state.setModel,
});
export default connect(mapStateToProps)(ModelComponent);
