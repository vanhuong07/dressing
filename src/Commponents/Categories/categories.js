import React, { Component } from "react";
import {connect} from 'react-redux';
class CategoriesComponent extends Component {
  render() {
    return (
      <>
        <div className='btn-group'>
            {this.props.CategoryLists.map((item,index) => 
            (<button onClick={() => this.filterCategory(item.type)} className={this.props.filterType === item.type ? 'btn btn-primary' : 'btn btn-secondary'}
             key={index}>{item.showName}</button>
            ))}
        </div>
      </>
    );
  }
  filterCategory = (payload) => {
    this.props.dispatch({
      type: 'FILTER_CLOTHES',
      payload
    })
  }
}

const mapStateToProps = (state) => ({
  CategoryLists : state.categories,
  filterType: state.filterType,
});


export default connect(mapStateToProps)(CategoriesComponent);
